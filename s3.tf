terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.62.0"
    }
  }
}
provider "aws" {
  region = "eu-west-1"
}

resource "aws_s3_bucket" "malka-sftp-bucket" {
  bucket = "malka-sftp-bucket"

  tags = {
    Name        = "sftp"
    Environment = "Dev"
  }
}

# "bucket_arn" {
#  value = aws_s3_bucket.malka-sftp-bucket.arn
#}
