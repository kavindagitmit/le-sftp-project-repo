#create aws transfer family server

resource "aws_transfer_server" "p8-sftp-server" {
  
  security_policy_name = "TransferSecurityPolicy-2020-06"
  endpoint_type = "VPC"

  endpoint_details {
    address_allocation_ids = [aws_eip.sftp-eip-1.id,aws_eip.sftp-eip-2.id]
    subnet_ids = [aws_subnet.sftp_subnet_az1.id,aws_subnet.sftp_subnet_az2.id]
    vpc_id     = aws_vpc.sftp_vpc.id
  }

  protocols   = ["SFTP"]
  #certificate = aws_acm_certificate.example.arn

  identity_provider_type = "SERVICE_MANAGED"
 #security_group_ids     = aws_security_group.sftp-sg.id
  domain                 = "S3"
  logging_role           = aws_iam_role.sftp_role.arn
}
resource "aws_transfer_tag" "hostname" {
     resource_arn = aws_transfer_server.p8-sftp-server.arn
     key = "aws:transfer:customHostname"
     value = "le-sftp-host"
}
