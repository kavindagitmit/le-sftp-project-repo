#create vpc
resource "aws_vpc" "sftp_vpc" {
  cidr_block       = "172.27.0.0/26"
  tags = {
    Name = "p8_sftp_vpc"
  }
}

#create igw
resource "aws_internet_gateway" "sftp-igw" {
  vpc_id = aws_vpc.sftp_vpc.id

  tags = {
    Name = "sftp-igw"
  }
}

#create subnets

resource "aws_subnet" "sftp_subnet_az1" {
  vpc_id     = aws_vpc.sftp_vpc.id
  cidr_block = "172.27.0.0/27"
  availability_zone = "eu-west-1a"
  
  tags = {
    Name = "sftp_subnet_az1"
  }
}

resource "aws_subnet" "sftp_subnet_az2" {
  vpc_id     = aws_vpc.sftp_vpc.id
  cidr_block = "172.27.0.32/27"
  availability_zone = "eu-west-1b"
  
  tags = {
    Name = "sftp_subnet_az2"
  }
}

#create network ACL
resource "aws_network_acl" "sftp-nacl-1" {
  vpc_id = aws_vpc.sftp_vpc.id

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }
  egress {
    protocol   = "tcp"
    rule_no    = 102
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = "tcp"
    rule_no    = 102
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "sftp-nacl-1"
  }
}

resource "aws_network_acl" "sftp-nacl-2" {
  vpc_id = aws_vpc.sftp_vpc.id

  egress {
    protocol   = "tcp"
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  egress {
    protocol   = "tcp"
    rule_no    = 102
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  ingress {
    protocol   = "tcp"
    rule_no    = 102
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "sftp-nacl-2"
  }
}

#NACL association

resource "aws_network_acl_association" "a" {
  network_acl_id = aws_network_acl.sftp-nacl-1.id
  subnet_id      = aws_subnet.sftp_subnet_az1.id
}

resource "aws_network_acl_association" "b" {
  network_acl_id = aws_network_acl.sftp-nacl-2.id
  subnet_id      = aws_subnet.sftp_subnet_az2.id
}

#create route tables

resource "aws_route_table" "sftp-rt-1" {
  vpc_id = aws_vpc.sftp_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.sftp-igw.id
  }

  tags = {
    Name = "sftp-rt-1"
  }
}

resource "aws_route_table" "sftp-rt-2" {
  vpc_id = aws_vpc.sftp_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.sftp-igw.id
  }

  tags = {
    Name = "sftp-rt-2"
  }
}

#route table subnet association

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.sftp_subnet_az1.id
  route_table_id = aws_route_table.sftp-rt-1.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.sftp_subnet_az2.id
  route_table_id = aws_route_table.sftp-rt-2.id
}

#create security group

resource "aws_security_group" "sftp-sg" {
  name        = "sftp-sg"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.sftp_vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.sftp_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sftp-sg"
  }
}

#create EIPs
resource "aws_eip" "sftp-eip-1" {
  vpc      = true
  tags = {
    Name = "sftp-eip-1"
  }
}

resource "aws_eip" "sftp-eip-2" {
  vpc      = true
  tags = {
    Name = "sftp-eip-2"
  }
}


