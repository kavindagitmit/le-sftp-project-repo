resource "aws_iam_role_policy" "sftp_policy" {
  name = "sftp_policy"
  role = aws_iam_role.sftp_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:ListBucket*",
        ]
        Effect   = "Allow"
        Resource = aws_s3_bucket.malka-sftp-bucket.arn
      },
      {
        Effect: "Allow",
            Action: [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion",
                "s3:GetObjectVersion",
                "s3:GetObjectACL",
                "s3:PutObjectACL"
            ],
            Resource = "${aws_s3_bucket.malka-sftp-bucket.arn}/*"
      }
    ]
  })
}

#depends_on = [
#aws_s3_bucket.malka-sftp-bucket
#]

resource "aws_iam_role" "sftp_role" {
  name = "sftp_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "transfer.amazonaws.com"
        }
      },
    ]
  })
}


