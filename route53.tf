variable "host_name" {
  type        = string
  description = "hostname"
  default     = "abc"
}

variable "zone" {
  type        = string
  description = "zone_name"
  default     = "lesftp.com"
}

#data "aws_transfer_server" "p8-sftp-server" {
#  server_id = aws_transfer_server.p8-sftp-server.id
#}

resource "aws_route53_zone" "sftp" {
  name = var.zone

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

resource "aws_route53_record" "hostname" {

  depends_on = [aws_route53_zone.sftp,aws_transfer_server.p8-sftp-server]
  zone_id = aws_route53_zone.sftp.id
  #zone_id = var.hosted_zone_id
  name    = var.host_name
  type    = "A"

  ttl = 300
  records = [aws_eip.sftp-eip-1.public_ip,aws_eip.sftp-eip-2.public_ip]
}
